#!/bin/sh

# this script loads peertube password from the password store I use
# see https://www.passwordstore.org/

# adjust it with the path where your obs is
# launch it with arguments you would pass to obs (-m -p for example)

OBS_PATH=$HOME/obs-studio-portable/bin/64bit
PEERTUBE_PASSWORD=`pass mose/distrilab-tube`

cd $OBS_PATH
PEERTUBE_PASSWORD=$PEERTUBE_PASSWORD ./obs $@
