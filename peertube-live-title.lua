obs = obslua
client_id = ''
client_secret = ''
expiration = 0
access_token = ''
-- properties
title_source = ''
tags_source = ''
desc_source = ''
peertube_url = ''
peertube_login = ''
peertube_live_id = ''


function os.capture(cmd)
  local f = assert(io.popen(cmd, 'r'))
  local s = assert(f:read('*a'))
  f:close()
  return s
end

function refresh_client()
  print("refreshing client ...")
  if client_id == '' then
    local client = os.capture('curl ' .. peertube_url .. '/api/v1/oauth-clients/local')
    local payload_client = obs.obs_data_create_from_json(client)
    client_id = obs.obs_data_get_string(payload_client, 'client_id')
    client_secret = obs.obs_data_get_string(payload_client, 'client_secret')
    print("(refreshed)")
  else
    print("(no need)")
  end
end

function get_token()
  refresh_client()
  print("refreshing token ...")
  if expiration == 0 or os.time() > expiration then
    local curl = string.format('curl -X POST -d "client_id=%s&client_secret=%s&grant_type=password&response_type=code&username=%s&password=%s" %s/api/v1/users/token', client_id, client_secret, peertube_login, os.getenv("PEERTUBE_PASSWORD"), peertube_url)
    local token_curl = os.capture(curl)
    local payload_token = obs.obs_data_create_from_json(token_curl)
    access_token = obs.obs_data_get_string(payload_token, 'access_token')
    local expires_in = obs.obs_data_get_int(payload_token, 'expires_in')
    expiration = os.time() + expires_in
    print("(refreshed)")
  else
    print("(no need)")
  end
end

function get_source_text(source_name)
  local source = obs.obs_get_source_by_name(source_name)
  local settings = obs.obs_source_get_settings(source)
  local content = obs.obs_data_get_string(settings, "text")
  obs.obs_source_release(source)
  obs.obs_data_release(settings)
  return content
end

function update_peertube_keypress(pressed)
  if pressed then
    update_peertube()
  end
end

function update_peertube()
  get_token()
  local title = get_source_text(title_source)
  local desc = get_source_text(desc_source)
  local tags = get_source_text(tags_source)

  local payload = obs.obs_data_create()
  obs.obs_data_set_string(payload, 'name', get_source_text(title_source))
  obs.obs_data_set_string(payload, 'description', get_source_text(desc_source))
  obs.obs_data_set_string(payload, 'tags', "TAGSHERE")

  local tags_str = get_source_text(tags_source)
  local payload_curl = obs.obs_data_get_json(payload)
  payload_curl = payload_curl:gsub('"TAGSHERE"', tags_str)

  local tmp_payload_file = os.tmpname()
  local file = io.open(tmp_payload_file, "w+")
  io.output(file)
  io.write(payload_curl)
  io.close(file)
  local curl = string.format('curl -X PUT -d @%s -H "Content-Type: application/json" -H "Authorization: Bearer %s" %s/api/v1/videos/%s', tmp_payload_file, access_token, peertube_url, peertube_live_id)
  local response_curl = os.capture(curl)
  
  if response_curl ~= '' then
    print(response_curl)
  end
  os.remove(tmp_payload_file)
end

function retrieve_peertube_info()
  get_token()
  print("retrieving info ...")
  local curl = string.format('curl -H "Authorization: Bearer %s" %s/api/v1/videos/%s | jq ". | {name,tags}"', access_token, peertube_url, peertube_live_id)
  local payload_curl = os.capture(curl)
  local file = io.open(peertube_live_id .. '-name.json', "w+")
  io.output(file)
  io.write(payload_curl)
  io.close(file)

  -- hack for getting description
  -- os.execute('echo -n \'{"description":\' > ' .. peertube_live_id .. '-desc.json')
  os.execute(string.format('curl -H "Authorization: Bearer %s" %s/api/v1/videos/%s/description > ' .. peertube_live_id .. '-desc.json', access_token, peertube_url, peertube_live_id))
  -- os.execute('echo -n "}" >> ' .. peertube_live_id .. '-desc.json')

  os.execute("jq -s '.[0] * .[1]' " .. peertube_live_id .. "-name.json " .. peertube_live_id .. "-desc.json > " .. peertube_live_id .. ".json")
  print("(retrieved)")
end

function retrieve_peertube_info_keypress(pressed)
  if pressed then
    retrieve_peertube_info()
  end
end

function restore_peertube_info()
  get_token()
  print("restoring info ...")
  local curl = string.format('curl -X PUT -d @%s -H "Content-Type: application/json" -H "Authorization: Bearer %s" %s/api/v1/videos/%s', peertube_live_id .. '.json', access_token, peertube_url, peertube_live_id)
  local response_curl = os.capture(curl)
  if response_curl ~= '' then
    print(response_curl)
  else
    print("(done)")
  end
end

function restore_peertube_info_keypress(pressed)
  if pressed then
    restore_peertube_info()
  end
end

function stream_start()
  retrieve_peertube_info()
  update_peertube()
end

function stream_stop()
  restore_peertube_info()
end

function on_event(event)
  if event == obs.OBS_FRONTEND_EVENT_STREAMING_STARTING then
    print("stream started")
    stream_start()
  elseif event == obs.OBS_FRONTEND_EVENT_STREAMING_STOPPING then
    print("stream stopped")
    stream_stop()
  end
end

----------------------------------------------------------

function script_description()
  return "This script is intended to make possible to update a peertube permanent live from OBS ...<p>\
  <hr><blockquote><b>Note</b>: the peertube password needs to be set in ENV<p>\
  see <a href=\"https://codeberg.org/mose/obs-peertube-live-title/src/branch/main/SETUP_ENV.md\">SETUP_ENV</a> for more info.<p>\
  Also, if you change peertube url, you need to reload the script.<p>\
  Dependency: linux, curl and jq\
  </blockquote>\
  <hr>"
end

function script_properties()
  local props = obs.obs_properties_create()

  local titlep = obs.obs_properties_add_list(props, "title_source", "Title Source", obs.OBS_COMBO_TYPE_EDITABLE, obs.OBS_COMBO_FORMAT_STRING)
  local tagsp = obs.obs_properties_add_list(props, "tags_source", "Tags Source", obs.OBS_COMBO_TYPE_EDITABLE, obs.OBS_COMBO_FORMAT_STRING)
  local descp = obs.obs_properties_add_list(props, "desc_source", "Description Source", obs.OBS_COMBO_TYPE_EDITABLE, obs.OBS_COMBO_FORMAT_STRING)

  local sources = obs.obs_enum_sources()
  if sources ~= nil then
    for _, source in ipairs(sources) do
      source_id = obs.obs_source_get_unversioned_id(source)
      if source_id == "text_gdiplus" or source_id == "text_ft2_source" then
        local name = obs.obs_source_get_name(source)
        obs.obs_property_list_add_string(titlep, name, name)
        obs.obs_property_list_add_string(tagsp, name, name)
        obs.obs_property_list_add_string(descp, name, name)
      end
    end
  end
  obs.source_list_release(sources)

  local peertube_url = obs.obs_properties_add_text(props, "peertube_url", "URL Peertube", obs.OBS_TEXT_DEFAULT)
  local peertube_login = obs.obs_properties_add_text(props, "peertube_login", "Login Peertube", obs.OBS_TEXT_DEFAULT)
  local peertube_live_id = obs.obs_properties_add_text(props, "peertube_live_id", "Live Id", obs.OBS_TEXT_DEFAULT)

  return props
end

function script_defaults(settings)
end

function script_update(settings)
  title_source = obs.obs_data_get_string(settings, "title_source")
  tags_source = obs.obs_data_get_string(settings, "tags_source")
  desc_source = obs.obs_data_get_string(settings, "desc_source")
  peertube_url = obs.obs_data_get_string(settings, "peertube_url")
  peertube_login = obs.obs_data_get_string(settings, "peertube_login")
  peertube_live_id = obs.obs_data_get_string(settings, "peertube_live_id")
end

function script_save(settings)
  local hotkey_save_array = obs.obs_hotkey_save(hotkey_id)
  obs.obs_data_set_array(settings, "update_peertube_hotkey", hotkey_save_array)
  obs.obs_data_array_release(hotkey_save_array)

  local restore_hotkey_save_array = obs.obs_hotkey_save(restore_hotkey_id)
  obs.obs_data_set_array(settings, "update_peertube_restore_hotkey", restore_hotkey_save_array)
  obs.obs_data_array_release(restore_hotkey_save_array)

  local retrieve_hotkey_save_array = obs.obs_hotkey_save(retrieve_hotkey_id)
  obs.obs_data_set_array(settings, "update_peertube_retrieve_hotkey", retrieve_hotkey_save_array)
  obs.obs_data_array_release(retrieve_hotkey_save_array)
end

function script_load(settings)
  hotkey_id = obs.obs_hotkey_register_frontend("update_peertube", "Update Peertube", update_peertube_keypress)
  local hotkey_save_array = obs.obs_data_get_array(settings, "update_peertube_hotkey")
  obs.obs_hotkey_load(hotkey_id, hotkey_save_array)
  obs.obs_data_array_release(hotkey_save_array)

  restore_hotkey_id = obs.obs_hotkey_register_frontend("restore_peertube", "Restore Peertube info", restore_peertube_info_keypress)
  local restore_hotkey_save_array = obs.obs_data_get_array(settings, "update_peertube_restore_hotkey")
  obs.obs_hotkey_load(restore_hotkey_id, restore_hotkey_save_array)
  obs.obs_data_array_release(restore_hotkey_save_array)

  retrieve_hotkey_id = obs.obs_hotkey_register_frontend("retrieve_peertube", "Retrieve Peertube info", retrieve_peertube_info_keypress)
  local retrieve_hotkey_save_array = obs.obs_data_get_array(settings, "update_peertube_retrieve_hotkey")
  obs.obs_hotkey_load(retrieve_hotkey_id, retrieve_hotkey_save_array)
  obs.obs_data_array_release(retrieve_hotkey_save_array)

  obs.obs_frontend_add_event_callback(on_event)

  if peertube_url ~= '' then
    local client = os.capture('curl ' .. peertube_url .. '/api/v1/oauth-clients/local')
    local payload_client = obs.obs_data_create_from_json(client)
    client_id = obs.obs_data_get_string(payload_client, 'client_id')
    client_secret = obs.obs_data_get_string(payload_client, 'client_secret')
  end
  print('script loaded')
end
