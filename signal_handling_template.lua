
-- cd = calldata object ... c'est géré par OBS
function signal1callback(cd)

end

function signal2callback(cd)

end


function script_load(settings)
    local handler = obs.obs_get_signal_handler()
    obs.signal_handler_connect(handler, "signal1", signal1callback)
    obs.signal_handler_connect(handler, "signal2", signal2callback)
end