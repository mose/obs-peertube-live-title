OBS Peertube live title Script
==================================

This LUA script is intended to make possible to update a peertube permanent live metadata from OBS:

- specify name, tags and description (name-desc) as text sources (can be in a scene that won't be used in live)
- when stream starts:
  - peertube API is called to get the out-of-live name-desc data, it is stored in a file for in case OBS crashes
  - name-desc is stored in a var in the script
  - have name-desc updated according to the source when stream starts (event OBS_FRONTEND_EVENT_STREAMING_STARTING)
- have a keyboard shortcut to sync peertube name-desc according to OBS scene content (for in case of in-live changes)
- when stream ends:
  - peertube API is called to restore the out-of-live name-desc from stored var
- if OBS crashes, we need to be able to restore name-desc from stored file with a keybinding

Docs RTFM
------------

- https://docs.joinpeertube.org/api-rest-reference.html
- https://obsproject.com/wiki/Getting-Started-With-OBS-Scripting
- https://obsproject.com/docs/scripting.html
- https://obsproject.com/forum/threads/tips-and-tricks-for-lua-scripts.132256/
- http://www.lua.org/manual/5.1/
- https://github.com/Chriscodinglife/get-started-with-lua
- https://www.tutorialspoint.com/execute_lua_online.php

Test cases
-------------

- [x] stream starts: stream info updated
- [x] stream stops: stream info from before stream updated
- [x] stream crash: restore initial stream info with keybinding
- [x] during stream: keybinding to update stream info

Todo
------------

- show an optional alert when stream info is updated
- show alert on error in api calls
- find a way to update thumbnail of video
- post notification of name change to some external channel (matrix, mastodon, etc)
- add timestamp to log output
- syntax checker on json fields
- write install instuctions
- move live info storage to a temp file

Credits
--------

This piece of code is public domain, and [WTFPL](http://www.wtfpl.net/txt/copying/). It was a collaborative effort from mose, tytan652, shinobi, 12b, greencoder, subreptice and others that participated in the https://live.mose.dev daily lives, where live coding happened.

Fuck the copyright. Information wants to be free.
